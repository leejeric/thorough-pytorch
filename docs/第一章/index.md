# 第一章：PyTorch的简介和安装
```{toctree}
:maxdepth: 2
1.1 PyTorch简介及安装
1.2 Tensor简介
1.3 自动求导机制.md
```